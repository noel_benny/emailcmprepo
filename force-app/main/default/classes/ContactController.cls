public with sharing class ContactController {

    /*
@ Class name        :   ContactController.cls
@ Created by        :   Noel Benny
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 003
@ Description       :   fetchContacts is for fetching contacts from the database Where Email ids are not empty. 
*/
    @AuraEnabled
    public static List<SObject> fetchContacts(string objectApiName, string fieldsetApiName){
        try{
            String query = 'SELECT ';
            
            for(FieldWrapper f : getFieldSetMembers(objectApiName, fieldsetApiName)) {
    
                query += f.fieldName+ ', ';    
            }
            query += 'Id FROM '+objectApiName+' WHERE Email != null LIMIT 100' ;
            List<Contact> lstStud = Database.query(query); 
            return lstStud;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

       /*
@ Class name        :   ContactController.cls
@ Created by        :   Noel Benny
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 003
@ Description       :   getFieldSetMembers is for fetching Fields depending on the FieldSet. 
*/
    @AuraEnabled
    public static List<FieldWrapper> getFieldSetMembers(string objectApiName, string fieldsetApiName){
        List<FieldWrapper> lstFieldWrapper = new List<FieldWrapper> ();

        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectApiName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldsetApiName);
        List<Schema.fieldSetMember> fieldSetMemberList = fieldSetObj.getFields();
        for (Schema.fieldSetMember objField : fieldSetMemberList) {

            lstFieldWrapper.add( new FieldWrapper(objField.getLabel(), objField.getfieldPath(), castTypeToString(objField.getType())));
        }
        return lstFieldWrapper;
    }

    public static String castTypeToString(Schema.DisplayType inputType){

        switch on inputType{
            when STRING{
                return 'STRING';
            }
            when BOOLEAN{
                return 'CHECKBOX';
            }
            when PICKLIST{
                return 'PICKLIST';
            }
            When INTEGER{
                return 'NUMBER';
            }
            When DOUBLE{
                return 'NUMBER';
            }
            When PERCENT{
                return 'NUMBER';
            }
            When ID{
                return 'ID';
            }
            When DATE{
                return 'DATE';
            }
            When DATETIME{
                return 'DATETIME';
            }
            When TIME{
                return 'TIME';
            }
            When URL{
                return 'URL';
            }
            When EMAIL{
                return 'EMAIL';
            }
            When PHONE{
                return 'PHONE';
            }
            When LONG{
                return 'LONG';
            }
            When MULTIPICKLIST{
                return 'MULTIPICKLIST';
            }
            When ENCRYPTEDSTRING{
                return 'ENCRYPTEDSTRING';
            }
            when else{
                return '';
            }
        }

    }
        /*
@ Class name        :   ContactController.cls
@ Created by        :   Noel Benny
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 003
@ Description       :   sendEmailController is to send the Mail and update the field lastMailSend . 
*/ 

    @AuraEnabled
    public static void sendEmailController(String emailDetailStr) {
        DateTime currentDateAndTime = datetime.now();
        List<Contact> lstToUpdate = new List<Contact>();
        EmailWrapper emailDetails = (EmailWrapper) JSON.deserialize(emailDetailStr, EmailWrapper.class);
        Messaging.reserveSingleEmailCapacity(1);
        try {
            messaging.SingleEmailMessage mail = new messaging.SingleEmailMessage();
            mail.setToAddresses(emailDetails.toAddress);
            mail.setCcAddresses(emailDetails.ccAddress);
            mail.setBccAddresses(emailDetails.bccAddress);
            mail.setSenderDisplayName('Test');
            mail.setSubject(emailDetails.subject);
            mail.setHtmlBody(emailDetails.body);
            /* mail.setEntityAttachments(emailDetails.files);  */
            /* Messaging.sendEmail(new List<messaging.SingleEmailMessage>{ mail }); */ 
           
            for (Id contactId : emailDetails.lstId) {
                Contact objContact = new contact(Id = contactId, Last_Email_Sent_Date_Time__c = currentDateAndTime );
                lstToUpdate.add(objContact);         
            }

            update lstToUpdate; 
        } catch (exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class EmailWrapper{

        
        @AuraEnabled public List<String> toAddress {get; set;}
        @AuraEnabled public List<String> ccAddress {get; set;}
        @AuraEnabled public List<String> bccAddress {get; set;}
        @AuraEnabled public String subject  {get; set;}
        @AuraEnabled public String body {get; set;}
        @AuraEnabled public List<Id> lstId {get; set;}
    
        EmailWrapper(List<String> toAddress, List<String> ccAddress, List<String> bccAddress, String subject, String body,List<Id> lstId){
            
            this.toAddress = toAddress;
            this.ccAddress = ccAddress;
            this.bccAddress = bccAddress;
            this.subject = subject;
            this.body = body;
            this.lstId = lstId;
        }

    }

    public class FieldWrapper{

        
        @AuraEnabled public string label {get; set;}
        @AuraEnabled public string fieldName {get; set;}
        @AuraEnabled public string type {get; set;}
        @AuraEnabled public Boolean sortable {get; set;}


        FieldWrapper(String label, String fieldName, String type ){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;
            this.sortable= true;
        }
     
    }    
}