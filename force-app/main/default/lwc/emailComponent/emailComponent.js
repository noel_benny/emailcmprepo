import { LightningElement, track, api } from 'lwc';
import fetchContacts from '@salesforce/apex/ContactController.fetchContacts';
import getFieldSetMembers from '@salesforce/apex/ContactController.getFieldSetMembers';
import sendEmailController from '@salesforce/apex/ContactController.sendEmailController';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class EmailComponent extends LightningElement {

    @track data = [];
    invalidEmailCc = [];
    invalidEmailBcc = [];
    @track toAddress = [];
    @track ccAddress;
    bccAddress;
    listbccAddress = [];
    @track listOfId = [];
    @track subject;
    @track body;
    @api objectApiName;
    @api fieldsetApiName;
    @track objEmail = {};
    @track selected = [];
    @track isModalOpen = false;
    @track listccAddress = [];
    @track sortBy;
    @track sortDirection;
    @track columns = [];


    connectedCallback() {
        console.log('>>>>>>>>>>inside connectedCallback');
        this.getFieldSetMembers();
    }

    doSorting(event) {
        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sortData(this.sortBy, this.sortDirection);
    }

    sortData(fieldname, direction) {
        let parseData = JSON.parse(JSON.stringify(this.data));
        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };
        // cheking reverse direction
        let isReverse = direction === 'asc' ? 1 : -1;
        // sorting data
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';
            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });
        this.data = parseData;

    }


    fetchContacts() {
        fetchContacts({
            objectApiName: this.objectApiName,
            fieldsetApiName: this.fieldsetApiName
        })
            .then(result => {
                this.data = result;

            }).catch(error => {
                console.log(error);
            })
    }

    getFieldSetMembers() {
        getFieldSetMembers({
            objectApiName: this.objectApiName,
            fieldsetApiName: this.fieldsetApiName
        })
            .then(result => {
                this.columns = result;
                this.fetchContacts();
            }).catch(error => {
                console.log(error);
            })

    }

    handleComposeEmail() {
        var el = this.template.querySelector('lightning-datatable');
        console.log(el);
        var selected = el.getSelectedRows();
        this.selected = selected;

        console.log(JSON.parse(JSON.stringify(selected)));

        if (this.selected.length != 0) {

            this.isModalOpen = true;

            for (let index = 0; index < this.selected.length; index++) {

                console.log('inside loop', this.selected[index].Email);

                this.toAddress.push(this.selected[index].Email);
                this.listOfId.push(this.selected[index].Id);

            }

        } else {
            this.showNotification('Error', 'Please Select Contacts', 'Error');
        }

    }
    validateEmailHandler() {
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

        if (this.ccAddress != null && this.ccAddress != '') {

            this.listccAddress = this.ccAddress.split(' ');
            this.listccAddress.forEach(element => {
                console.log(element);

                if (emailPattern.test(element) == false) {
                    this.invalidEmailCc.push(element);
                }

            });
            if (this.invalidEmailCc.length != 0) {
                this.showNotification('Error', 'Please Enter Valid Emails Sperated by Space', 'Error');

            }
        }
        if (this.bccAddress != null && this.bccAddress != '') {
            this.listbccAddress = this.bccAddress.split(' ');
            this.listbccAddress = this.bccAddress.split(' ');
            this.listbccAddress.forEach(element => {

                if (emailPattern.test(element) == false) {
                    this.invalidEmailBcc.push(element);
                }

            });
        }
        if (this.invalidEmailBcc.length != 0) {
            this.showNotification('Error', 'Please Enter Valid Emails Sperated by Space', 'Error');
        }
        if ((this.invalidEmailBcc.length == 0) && (this.invalidEmailCc.length == 0)) {
            this.sendEmailController();

        }


    }

    sendEmailController() {

        let emailDetails = {
            toAddress: this.toAddress,
            ccAddress: this.listccAddress,
            bccAddress: this.listbccAddress,
            subject: this.subject,
            body: this.body,
            lstId: this.listOfId
        };

        sendEmailController({ emailDetailStr: JSON.stringify(emailDetails) },
        )
            .then(() => {
                this.showNotification('Success', 'Email Sent', 'Success');
                this.fetchContacts();
                this.template.querySelector('lightning-datatable').maxRowSelection = 0;
                this.template.querySelector('lightning-datatable').maxRowSelection = 100;
                this.cancelHandler();
            })
            .catch((error) => {
                this.showNotification('Error', 'Email Failed ' + error, 'Error');
            });

    }
    cancelHandler() {
        this.isModalOpen = false;
        this.ccAddress = null;
        this.bccAddress = null;
        this.subject = null;
        this.body = null;
        this.listOfId = [];

    }

    handlecloseModal() {

        this.isModalOpen = false;
        this.cancelHandler();

    }
    inputhandler(event) {
        let fieldName = event.target.name;
        let value = event.target.value;
        if (fieldName == 'ccAddress') {

            this.ccAddress = '' + value;

        } else if (fieldName == 'subject') {

            this.subject = value;

        } else if (fieldName == 'body') {

            this.body = value;

        } else if (fieldName == 'bccAddress') {
            this.bccAddress = '' + value;
        }

    }
    sendMailHandler() {

        this.invalidEmailCc = [];
        this.invalidEmailBcc = [];


        if (this.body == null || this.body == '') {
            this.showNotification('Error', 'Email Body Cannot Be Empty', 'Error');
        }
        else {
            this.validateEmailHandler();

        }

    }

    showNotification(title, message, variant) {
        console.log("inside  showNotification()");
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(evt);
    }


}